﻿public class Odd_Prime_Number
{
   public static bool isOdd(int number)
    {
        return (number % 2 == 0);
    }

    public static bool isPrime(int number)
    {
        for (int i = 2; i < number; i++)
        {
            if (number%i==0)
            {
                return false;
            }
        }
        return true;
    }

}
