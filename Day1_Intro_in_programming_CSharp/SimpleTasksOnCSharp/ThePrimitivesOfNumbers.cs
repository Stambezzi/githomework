﻿class ThePrimitivesOfNumbers
{
    static void Main()
    {
        //how to print array without loops
        int[] a = { 1, 2, 3 };
        Console.WriteLine("My array: " + "[{0}]", string.Join(", ", a));

        Console.WriteLine(Int64.MaxValue.ToString());
        Console.WriteLine(Int16.MaxValue.ToString());
        Console.WriteLine(Int32.MaxValue.ToString());
        Console.WriteLine(Int64.MinValue.ToString());
    }
}