﻿using System;

public class Program
{
    static void Swap(int[] arr, int pos, int prevPos)
    {
        if (arr[pos] != arr[prevPos])
        {
            arr[pos] ^= arr[prevPos];
            arr[prevPos] ^= arr[pos];
            arr[pos] ^= arr[prevPos];
        }
    }

    static void BubbleSort(int[] array, uint size)
    {
        bool changed;
        uint sorted = 0;
        do
        {
            changed = false;
            for (uint i = size - 1; i > sorted; --i)
            {
                if (array[i] < array[i - 1])
                {
                    Swap(array, (int)i, (int)(i - 1));
                    changed = true;
                }
            }
            ++sorted;
        } while (changed);
    }

    public static int ReturnMissingNumber(int[] arr, int size)
    {
        int result = -1;
        BubbleSort(arr, (uint)size);
        for (int i = 1; i < size; i++)
        {
            if (Math.Abs(arr[i - 1] - arr[i]) > 1)
            {
                result = arr[i - 1] + 1;
                break;
            }
        }
        return result;
    }

    static void Main()
    {
    }
}