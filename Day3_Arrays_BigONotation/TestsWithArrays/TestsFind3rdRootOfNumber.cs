
using Microsoft.VisualStudio.TestPlatform.TestHost;

namespace TestsFind3rdRootOfNumber
{
    [TestClass]
    public class TestsFind3rdRootOfNumber
    {
        [TestMethod]
        public void TestMethod1()
        {
            int given = 27;
            int expected = 3;
            int actual = Find3rdRoot.Find3rdRootOfNumber(given);
            Assert.AreEqual(actual, expected);
        }
        [TestMethod]
        public void TestMethod2()
        {
            int given = 1;
            int expected = 1;
            int actual = Find3rdRoot.Find3rdRootOfNumber(given);
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void TestMethod3()
        {
            int given = 2744;
            int expected = 14;
            int actual = Find3rdRoot.Find3rdRootOfNumber(given);
            Assert.AreEqual(actual, expected);
        }
    }
}