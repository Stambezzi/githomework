﻿namespace OOPFileSystem
{
    public class Folder
    {
        private string nameOfFolder;
        private string path;

        Dictionary<string, Folder> folders;
        Dictionary<string, File> files;

        public Folder(string currentPath, string name)
        {
            this.path = currentPath + "\\" + name;
            this.nameOfFolder = name;
            folders = new Dictionary<string, Folder>();
            files = new Dictionary<string, File>();
        }

        public string Name
        {
            get { return nameOfFolder; }

            set
            {
                nameOfFolder = value;
                string[] parts = path.Split('\\');
                if (parts.Length > 0)
                {
                    parts[parts.Length - 1] = value;
                    path = string.Join("\\", parts);
                }
            }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public Dictionary<string, Folder> Folders
            => folders;


        public Dictionary<string, File> Files
        {
            get { return files; }
        }

        public string Ls()
        {
            string output = "";
            foreach (var folder in folders)
            {
                output += folder.Key + "\\" + "\t";
            }

            foreach (var file in files)
            {
                output += file.Key + "\t";
            }

            return output;
        }

        //creating a folder is available only in the current dirrectory
        public void Mkdir(string folderName)
        {
            Folder newFolder = new(path, folderName);
            folders.Add(folderName, newFolder);
        }

        public void CreateFile(string filename)
        {
            File newFile = new(path, filename);
            files.Add(filename, newFile);
        }
    }
}
