﻿using System;

namespace OOPFileSystem
{
    public class FileSystem
    {
        private Folder homeFolder;
        private Folder currentFolder;

        public FileSystem()
        {
            homeFolder = new Folder("", "home");
            currentFolder = homeFolder;
        }

        private bool Cd_helper(string path, Folder currentFolder)
        {
            foreach (var item in currentFolder.Folders)
            {
                if (path == item.Key)
                {
                    currentFolder = item.Value;
                    return true;
                }
                else
                {
                    return true || Cd_helper(path, item.Value);
                }

            }
            return false;
        }

        public void Cd(string path)
        {
            //case current folder, should make it work with .
            string[] parts = path.Split('\\');
            if (parts[0] == "." || (parts.Length == 1))
            {
                if (currentFolder.Folders != null)
                {
                    foreach (var item in currentFolder.Folders)
                    {
                        if (path == item.Key)
                        {
                            currentFolder = item.Value;
                            return;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Path doesn't exist");
                    return;
                }
            }
            //TODO
            //add implementation for .. or for the whole directory


            bool found = Cd_helper(path, currentFolder);



            if (found == false)
            {
                Console.WriteLine("Path doesn't exist, try again\n");
            }
        }

        public bool HandleInput(string command)
        {
            string[] commandArray = command.Split(' ');

            switch (commandArray[0])
            {
                case "pwd":
                    Console.WriteLine(currentFolder.Path);
                    break;
                case "cd":
                    Cd(commandArray[1]);
                    break;
                case "mkdir":
                    currentFolder.Mkdir(commandArray[1]);
                    break;
                case "create_file":
                    currentFolder.CreateFile(commandArray[1]);
                    break;
                case "cat":
                    break;
                case "tail":
                    break;
                case "write":
                    break;
                case "ls":
                    Console.WriteLine(currentFolder.Ls());
                    break;
                case "exit":
                    Console.WriteLine(">\tHave a nice day! =) \n");
                    return false;
                default:
                    Console.WriteLine(string.Format("Command {1} does not exist", commandArray[0]));
                    break;
            }
            return true;
        }
    }
}
