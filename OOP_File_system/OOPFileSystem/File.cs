﻿namespace OOPFileSystem
{
    public class File
    {
        private string path;
        private string filename;
        private int highestRow;
        private Dictionary<int, string> content;
        public File(string currentPath, string filename)
        {

            this.filename = filename;
            this.path = currentPath + "\\" + filename;
            highestRow = 0;
            content = new() { };

        }

        public void Write(int row, string contentToAdd)
        {
            if (this.content.ContainsKey(row))
            {
                this.content[row] += contentToAdd;
            }
            else
            {
                this.content.Add(row, contentToAdd);
            }

            if (row > highestRow)
            {
                highestRow = row;
            }
        }

        public string Cat()
        {
            return Tail(0);
        }

        public string Tail(int rows = 10)
        {
            int begining = Math.Max(highestRow - rows, 0);

            string contents = "";

            for (int i = begining; i <= highestRow; i++)
            {
                if (content.ContainsKey(i))
                {
                    contents += content[i];
                }
                contents += "\n";
            }

            return contents;
        }
    }
}
