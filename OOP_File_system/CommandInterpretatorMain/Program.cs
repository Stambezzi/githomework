﻿using OOPFileSystem;

namespace CommandInterpretatorMain
{
    internal class Program
    {
        static void Main(string[] args)
        {
           FileSystem fs = new FileSystem();
            string commandInput = "";
            bool currentlyExecuting = true;
            while (currentlyExecuting)
            {
                Console.Write("> ");
                commandInput = Console.ReadLine();
                currentlyExecuting = fs.HandleInput(commandInput);
            }
        }
    }
}