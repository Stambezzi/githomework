using Tree;

namespace TestTrees
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestingBFS()
        {
            List<int> expected = new(){ 10, 20, 30, 40, 50, 60, 70 };


            var node70 = new BinaryTreeNode(70, null, null);
            var node60 = new BinaryTreeNode(60, null, null);
            var node30 = new BinaryTreeNode(30, node60, node70);

            var node50 = new BinaryTreeNode(50, null, null);
            var node40 = new BinaryTreeNode(40, null, null);
            var node20 = new BinaryTreeNode(20, node40, node50);

            var binaryTree = new BinaryTreeNode(10, node20, node30);

            var bfs = new BreathFirstSearch();

            var result = bfs.Search(binaryTree);
            CollectionAssert.Equals(expected, result);
        }
    }
}