﻿using Tree;
public class BreathFirstSearch
{
    public List<int> Search(BinaryTreeNode root)
    {
        List<int> bfs_sequence = new List<int>();
        var elementsToTraverse = new Queue<BinaryTreeNode>();

        elementsToTraverse.Enqueue(root);

        while (elementsToTraverse.Count > 0)
        {
            var currentNode = elementsToTraverse.Dequeue();

            bfs_sequence.Add(currentNode.Value);

            if (currentNode.Left != null)
            {
                elementsToTraverse.Enqueue(currentNode.Left);
            }

            if (currentNode.Right != null)
            {
                elementsToTraverse.Enqueue(currentNode.Right);
            }
        }
        return bfs_sequence;
    }
}