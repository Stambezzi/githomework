﻿using System.Collections.Generic;

public class EraseMiddleLL
{
    public static void removeMidElementOfLinkedList(LinkedList<int> ll)
    {
        if (ll.Count == 0) return; // if count < 3 return; maybe

        var slow = ll.First;
        var fast = ll.First;

        while (fast.Next != null && fast.Next.Next != null) // you evaluate fast.Next twice her. can optimize it - try with while(fast.Next?.Next != null)
        {
            slow = slow.Next;
            fast = fast.Next.Next;
        }
        if (slow != null)
        {
            ll.Remove(slow);
        }
    }

    //static void Main()
    //{
        //LinkedList<int> given = new();
        //given.AddLast(5);
        //given.AddLast(2);
        //given.AddLast(5);
        //given.AddLast(4);
        //given.AddLast(3);

        //EraseMiddleLL.removeMidElementOfLinkedList(given);
        //LinkedList<int> result = new();
        //result.AddLast(5);
        //result.AddLast(2);
        //result.AddLast(4);
        //result.AddLast(3);

        //Console.WriteLine(result);
    //}

}

