﻿public class AnagramTask
{
    //got help for this one
    //find this solution so beatuful, you have a better one, please show it to me =)
    public static List<List<string>> returnListsOfAnagrams(string[] words)
    {
        Dictionary<string, List<string>> anagramGroups = new Dictionary<string, List<string>>();

        foreach (var word in words)
        {
            char[] characters = word.ToCharArray();
            // използвайки сортировка увеличаваш малко сложността
            Array.Sort(characters);
            string sortedWord = new string(characters);

            if (anagramGroups.ContainsKey(sortedWord))
            {
                anagramGroups[sortedWord].Add(word);
            }
            else
            {
                anagramGroups[sortedWord] = new List<string> { word };
            }
        }

        // иначе е супер задачата, от кого получи помощ?
        return anagramGroups.Values.ToList();
    }
}
