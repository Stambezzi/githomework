﻿using System.Collections;
public static class CarPlate
{


    public static Dictionary<string, string> CarPlateToOwner = new Dictionary<string, string>();//Plate, Owner
    public static KeyValuePair<string, string> returnCoolestNumberPlate()
    {
        KeyValuePair<string, string> coolestPlate = new KeyValuePair<string, string>();
        int counterOfLetters = 0;
        int index = 0;
        int prevValue = Int32.MinValue;

        for (int j = 0; j < CarPlateToOwner.Count; j++)
        {
            counterOfLetters = 0;

            for (int i = 0; i < CarPlateToOwner.ElementAt(j).Key.Length; i++)
            {
                if (prevValue < counterOfLetters)
                {
                    prevValue = counterOfLetters;
                    index = j;
                }
                if ('A' <= CarPlateToOwner.ElementAt(j).Key[i] && CarPlateToOwner.ElementAt(j).Key[i] <= 'Z')
                {
                    counterOfLetters++;
                }
            }
        }
        // Не разбирам по-горната логика, но търсейки по индекс, не се възползваш от най-голямата сила на хеш таблицата - че може да търсиш много бързо (константно).
        coolestPlate = CarPlateToOwner.ElementAt(index);
        return coolestPlate;
    }

    public static string getTheOwnerWithMostCars()
    {
        int counterOfCars = 0;
        int tempCount = 0;
        int index = -1;
        int tempIndex = 0;

        foreach (var item1 in CarPlateToOwner)
        {
            counterOfCars = 0;
            tempIndex = 0;
            foreach (var item2 in CarPlateToOwner)
            {
                if (item1.Value == item2.Value)
                {
                    tempCount++;
                }
                tempIndex++;
            }
            if (tempCount > counterOfCars)
            {
                counterOfCars = tempCount;
                index = tempIndex;
            }
        }

        // Целта тук е да върнеш всички собственици с повече от една кола, а не собственика с най-много коли
        // Ако все пак целта ти е да намериш собственика с най-много коли има по-оптимален начин - как?
        string owner =  CarPlateToOwner.ElementAt(index-1).Value;
        return owner;
    }


    public static void printDictionary()
    {
        Console.WriteLine("Cars currently in the list: ");
        foreach (KeyValuePair<string, string> plateOwner in CarPlateToOwner)
        {
            Console.WriteLine(string.Format("Car {0} is Owned by {1}",
                plateOwner.Key, plateOwner.Value));
        }
    }

    //public static void Main()
    //{

    //}
}
