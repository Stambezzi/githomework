﻿public static class IntersectionOfArrays
{
    public static int[] extractIntersection(int[] first, int[] second)
    {
        Dictionary<int, int> dic1 = new Dictionary<int, int>();
        Dictionary<int, int> dic2 = new Dictionary<int, int>();

        // не виждам никъде да го ползваш този резулт?
        Dictionary<int, int> result = new Dictionary<int, int>();

        // Дублицираш код, може да го изнесеш на една обща функция
        for (int i = 0; i < first.Length; i++)
        {
            if (dic1.ContainsKey(first[i]))
            {
                dic1[first[i]]++;
            }
            else
            {
                dic1.Add(first[i], 1);
            }
        }

        // Дублицираш код, може да го изнесеш на една обща функция
        for (int i = 0; i < second.Length; i++)
        {
            if (dic2.ContainsKey(second[i]))
            {
                dic2[second[i]]++;
            }
            else
            {
                dic2.Add(second[i], 1);
            }
        }

        // това е супер.
        var toReturn = dic1.Keys.Intersect(dic2.Keys).ToArray();
        return toReturn;
    }
}
