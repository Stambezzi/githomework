﻿using System.Linq;

public static class NonRepeatingChars
{

    public static List<char> getSetOfChars(string input)
    {
        Dictionary<char, int> setOfLetters = new();

        List<char> setOfChars = new();
        for (int i = 0; i < input.Length; i++)
        {
            if (setOfLetters.ContainsKey(input[i]))
            {
                setOfLetters[input[i]]++;
            }
            else
            {
                setOfLetters.Add(input[i], 1);
            }
        }


        foreach (var item in setOfLetters)
        {
            // грешна е проверката тук, трябва да търсиш всички символи, които се повтарят повече от веднъж, вместо всички които са букви
            if ('A' <= item.Key && item.Key <= 'Z' ||
            'a' <= item.Key && item.Key <= 'z')
            {

                setOfChars.Add(item.Key);
            }
        }

        // останалта част е абсолютно вярна
        return setOfChars;
    }
}

// не виждам другата подточка с  индекса на първия елемент, който е уникален
