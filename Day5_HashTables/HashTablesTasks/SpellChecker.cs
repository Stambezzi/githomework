﻿using System;
using System.Collections.Generic;

public static class SpellChecker
{
    public static string extractWordFromString(string sentence, ref int from)
    {
        string word = "";
        if (sentence.Length == 0)
        {
            return word;
        }

        while (from < sentence.Length && (sentence[from] == ' ' || sentence[from] == ',' || sentence[from] == '.' ||
            sentence[from] == '!' || sentence[from] == '?'))
        {
            from++;
        }

        while (from < sentence.Length && !(sentence[from] == ' ' || sentence[from] == ',' || sentence[from] == '.' ||
            sentence[from] == '!' || sentence[from] == '?'))
        {
            word += sentence[from];
            from++;
        }
        return word;
    }

    // Има вграден функция, която прави точно това, може да ползваш нея
    public static string[] extractWords(string sentence)
    {
        List<string> words = new List<string>();
        int from = 0;
        while (from < sentence.Length)
        {
            words.Add(extractWordFromString(sentence, ref from));
        }

        // а, ето ти си я октрил. Ползвай директно нещата които има в езика, няма смисъл да откриваш топлата вода отново :D
        //string[] words1 = sentence.Split(new char[] { ' ', ',', '.', '!', '?' },
        //    StringSplitOptions.RemoveEmptyEntries);

        return words.ToArray();
    }

    public static string extractMisspelledOrMissingWords(string sentence, HashSet<string> dic)
    {
        string[] words = extractWords(sentence);
        string missingOrMisspelledWords = "";
        foreach (string word in words)
        {
            if (!dic.Contains(word))
            {
                missingOrMisspelledWords += word + ", ";
            }
        }
        // единствената ми забележка тук е, че връщаш думите като стринг
        // доста по-добре би било да са като масив или като лист или дори сет
        return missingOrMisspelledWords;
    }

    public static List<string> extractMisspelledWords(string input, HashSet<string> words)
    {
        var result = new List<string>();

        return result;
    }
}

